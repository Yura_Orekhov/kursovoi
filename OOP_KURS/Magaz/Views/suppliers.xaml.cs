﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Store.Views
{
    using DAL;
    using ExtendedXmlSerialization;
    using Model;
    using System.Xml;

    /// <summary>
    /// Interaction logic for suppliers.xaml
    /// </summary>
    /// 
    using ViewModel;

    public partial class suppliers : UserControl
    {
        public suppliers()
        {
            InitializeComponent();
            
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

            DataContext = new SupplierViewModel();
        }

        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
           
        }
    }
}
