﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Model
{
    public class Supplier : Entity, IDataErrorInfo
    {
        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                NotifyPropertyChanged("Title");
            }
        }

        private string fio;
        public string Fio
        {
            get { return fio; }
            set
            {
                fio = value;
                NotifyPropertyChanged("Fio");
            }
        }

        private DateTime dateTime;
        public DateTime DateTime
        {
            get { return dateTime; }
            set
            {
                dateTime = value;
                NotifyPropertyChanged("DateTime");
            }
        }

        private string adress;
        public string Adress
        {
            get { return adress; }
            set
            {
                adress = value;
                NotifyPropertyChanged("Adress");
            }
        }

        private string phone;
        public string Phone
        {
            get { return phone; }
            set
            {
                phone = value;
                NotifyPropertyChanged("Phone");
            }
        }

        public List<Supply> Supplies { get; set; }

        public Supplier()
        {
            Supplies = new List<Supply>();
            DateTime = DateTime.Now;
        }

        public override string ToString()
        {
            return Title;
        }

        #region IDataError
        public string Error
        {
            get
            {
                return null;
            }
        }

        public string this[string columnName]
        {
            get { return GetValidationError(columnName); }
        }

        private static readonly string[] ValidatedProperties =
        {
            "Title", "Fio", "Adress", "Phone"
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string errorMsg = null;
            switch (propertyName)
            {
                case "Title":
                    if (string.IsNullOrEmpty(Title))
                        errorMsg = "Поле \"Назва фірми\" не може бути пустим";
                    break;
                case "Fio":
                    if (string.IsNullOrEmpty(Fio))
                        errorMsg = "Поле \"ПІБ керівника\" не може бути пустим";
                    break;
                case "Adress":
                    if (string.IsNullOrEmpty(Adress))
                        errorMsg = "Поле \"Адреса\" не може бути пустим";
                    break;
                case "Phone":
                    if (string.IsNullOrEmpty(Title))
                        errorMsg = "Поле \"Номер телефону\" не може бути пустим";
                    break;
            }
            return errorMsg;
        }
        #endregion
    }
}
