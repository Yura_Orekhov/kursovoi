﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Model
{
    public class Category : Entity, IDataErrorInfo
    {
        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public List<Good> Goods { get; set; }

        public Category()
        {
            Goods = new List<Good>();
        }

        public override string ToString()
        {
            return Title;
        }

        #region IDataError
        public string Error
        {
            get
            {
                return null;
            }
        }

        public string this[string columnName]
        {
            get { return GetValidationError(columnName); }
        }

        private static readonly string[] ValidatedProperties =
        {
            "Title"
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string errorMsg = null;
            switch (propertyName)
            {
                case "Title":
                    if (string.IsNullOrEmpty(Title))
                        errorMsg = "Поле \"Назва\" не може бути пустим";
                    break;
            }
            return errorMsg;
        }
        #endregion

    }
}
