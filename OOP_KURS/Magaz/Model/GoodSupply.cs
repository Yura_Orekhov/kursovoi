﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Model
{
    public class GoodSupply : Entity
    {
        private string goodTitle;
        public string GoodTitle
        {
            get { return goodTitle; }
            set
            {
                goodTitle = value;
                NotifyPropertyChanged("GoodTitle");
            }
        }

        private int goodId;
        public int GoodId
        {
            get { return goodId; }
            set
            {
                goodId = value;
                NotifyPropertyChanged("GoodId");
            }
        }


    }
}
