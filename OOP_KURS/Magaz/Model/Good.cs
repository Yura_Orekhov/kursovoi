﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Model
{
    public class Good : Entity, IDataErrorInfo
    {
        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                NotifyPropertyChanged("Title");
            }
        }

        private string maker;
        public string Maker
        {
            get { return maker; }
            set
            {
                maker = value;
                NotifyPropertyChanged("Maker");
            }
        }

        private string price;
        public string Price
        {
            get { return price; }
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }

        private string descripion;
        public string Description
        {
            get { return descripion; }
            set
            {
                descripion = value;
                NotifyPropertyChanged("Description");
            }
        }

        private int amount;
        public int Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                NotifyPropertyChanged("Amount");
            }
        }

        private Category category;
        public Category Category
        {
            get { return category; }
            set
            {
                category = value;
                NotifyPropertyChanged("Category");
            }
        }

        public List<Supply> Supplies { get; set; }

        public Good()
        {
            Supplies = new List<Supply>();
        }

        public override string ToString()
        {
            return Title + " " + Maker;
        }

        #region IDataError
        public string Error
        {
            get
            {
                return null;
            }
        }

        public string this[string columnName]
        {
            get { return GetValidationError(columnName); }
        }

        private static readonly string[] ValidatedProperties =
        {
            "Title", "Maker", "Price", "Description", "Category"
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string errorMsg = null;
            switch (propertyName)
            {
                case "Title":
                    if (string.IsNullOrEmpty(Title))
                        errorMsg = "Поле \"Назва\" не може бути пустим";
                    break;
                case "Maker":
                    if (string.IsNullOrEmpty(Maker))
                        errorMsg = "Поле \"Виробник\" не може бути пустим";
                    break;
                case "Description":
                    if (string.IsNullOrEmpty(Description))
                        errorMsg = "Поле \"Оис товара не може бути пустим\" не може бути пустим";
                    break;
                case "Price":
                    if (string.IsNullOrEmpty(Price))
                        errorMsg = "Поле \"Ціна\" не може бути пустим";
                    break;
                case "Category":
                    if (category == null)
                        errorMsg = "Поле \"Категорія\" не може бути пустим";
                    break;
            }
            return errorMsg;
        }
        #endregion
    }
}
