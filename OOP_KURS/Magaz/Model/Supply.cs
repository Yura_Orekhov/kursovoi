﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Model
{
    public class Supply : Entity, IDataErrorInfo
    {
        private Supplier supplier;
        public Supplier Supplier
        {
            get { return supplier; }
            set
            {
                supplier = value;
                NotifyPropertyChanged("Supplier");
            }
        }

        private double price;
        public double Price
        {
            get { return price; }
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }

     public List<GoodSupply> GoodSupply { get; set; }

        private DateTime dateTime;
        public DateTime DateTime
        {
            get { return dateTime; }
            set
            {
                dateTime = value;
                NotifyPropertyChanged("DateTime");
            }
        }

        public Supply()
        {
            DateTime = DateTime.Now;
            GoodSupply = new List<GoodSupply>();
        }

        #region IDataError
        public string Error
        {
            get
            {
                return null;
            }
        }

        public string this[string columnName]
        {
            get { return GetValidationError(columnName); }
        }

        private static readonly string[] ValidatedProperties =
        {
            "Supplier"
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string errorMsg = null;
            switch (propertyName)
            {
                case "Supplier":
                    if (Supplier == null)
                        errorMsg = "Поле \"Постачальник\" не може бути пустим";
                    break;
            }
            return errorMsg;
        }
        #endregion
    }
}
