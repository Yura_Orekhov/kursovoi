﻿using LiveCharts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.ViewModel
{
    using DAL;
    using LiveCharts;
    using LiveCharts.Wpf;
    using Model;
    using System.Collections.ObjectModel;

    class StatisticViewModel:ViewModelBase
    {
        private Storage storage = new Storage();

        private ObservableCollection<Good> goods;
        public ObservableCollection<Good> Goods
        {
            get { return goods; }
            set
            {
                goods = value;
                NotifyPropertyChanged("Goods");
            }
        }

        private void LoadGoods()
        {
            Goods = new ObservableCollection<Good>();
            List<Category> categories = storage.GetAllCategories();

            foreach (var cat in categories)
            {
                foreach (var g in cat.Goods)
                {
                    Goods.Add(g);
                }
            }
        }

public SeriesCollection SeriesCollection { get; set; }
public string[] Labels { get; set; }
public Func<double, string> Formatter { get; set; }

public StatisticViewModel()
        {

            LoadGoods();
            var brands = Goods.Select(x => x.Maker).Distinct().ToList();
            Dictionary<string, int> stat = new Dictionary<string, int>();


            foreach(var brand in brands)
            {
                int total = 0;
                foreach(var good in Goods)
                {
                    if (good.Maker == brand)
                        total += 1;
                }
                stat.Add(brand, total);
            }

            SeriesCollection = new SeriesCollection();

            //adding series will update and animate the chart automatically
            SeriesCollection.Add(new ColumnSeries
            {
                Values = new ChartValues<int>(stat.Values.ToArray())
            });

            Labels = stat.Keys.ToArray();
            Formatter = value => value.ToString("N");

        }
    }
    }

