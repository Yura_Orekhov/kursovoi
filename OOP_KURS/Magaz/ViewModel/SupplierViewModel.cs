﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.ViewModel
{
    using DAL;
    using Model;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;

    class SupplierViewModel : ViewModelBase
    {
        private Storage storage = new Storage();

        private Supplier newSupplier = new Supplier();
        public Supplier NewSupplier
        {
            get { return newSupplier; }
            set
            {
                newSupplier = value;
                NotifyPropertyChanged("NewSupplier");
            }
        }

        private ICommand addSupplierCommand;
        public ICommand AddSupplierCommand
        {
            get
            {
                if (addSupplierCommand == null)
                    addSupplierCommand = new RelayCommand(
                        p => AddSupplier(),
                        p => true
                        );
                return addSupplierCommand;
            }
        }

        private void AddSupplier()
        {
            if (!NewSupplier.IsValid)
            {
                MessageBox.Show(NewSupplier.Title);
                return;
            }
            bool isUnique = false;
            while (isUnique == false)
            {
                Random r = new Random();
                int id = r.Next(10000000, 99999999);
                var query = Suppliers.Where(p => p.ID == id).ToList();
                if (query.Count == 0)
                {
                    isUnique = true;
                    NewSupplier.ID = id;
                }
            }
            try
            {
                Suppliers.Add(NewSupplier);
                NewSupplier = new Supplier();
                storage.SaveSuppliers(Suppliers.ToList());
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }




        private ICommand evalPriceCommad;
        public ICommand EvalPriceCommand
        {
            get
            {
                if (evalPriceCommad == null)
                    evalPriceCommad = new RelayCommand(
                        p => EvalTotalCost(),
                        p => true
                        );
                return evalPriceCommad;
            }
        }

        private Supplier selectedSupplier;
        public Supplier SelectedSupplier
        {
            get { return selectedSupplier; }
            set
            {
                selectedSupplier = value;
                NotifyPropertyChanged("SelectedSupplier");
            }
        }

        private ICommand removeCommand;
        public ICommand RemoveCommand
        {
            get
            {
                if (removeCommand == null)
                    removeCommand = new RelayCommand(
                        p => Remove(),
                        p => true
                        );
                return removeCommand;
            }
        }

        private void Remove()
        {
            if (selectedSupplier == null) return;

            try
            {
                var suppliers = storage.GetAllSuppliers();
                var supplier = suppliers.Where(p => p.ID == selectedSupplier.ID).First();
                var flag = suppliers.Remove(supplier);
                Suppliers.Remove(SelectedSupplier);
                storage.SaveSuppliers(suppliers);
                MessageBox.Show("Постачальника видалено!!!");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private ICommand saveChangesCommand;
        public ICommand SaveChangesCommand
        {
            get
            {
                if (saveChangesCommand == null)
                    saveChangesCommand = new RelayCommand(
                        p => SaveChanges(),
                        p => true
                        );
                return saveChangesCommand;
            }
        }

        private void SaveChanges()
        {
            try
            {
                storage.SaveSuppliers(Suppliers.ToList());
                MessageBox.Show("Дані збережено!!!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private ICommand refreshCommand;
        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand == null)
                    refreshCommand = new RelayCommand(
                        p => Refresh(),
                         p => true
                        );
                return refreshCommand;
            }
        }

        private void Refresh()
        {
            Suppliers = new ObservableCollection<Supplier>(storage.GetAllSuppliers());
        }

        private void EvalTotalCost()
        {
            Dictionary<string, double> result = new Dictionary<string, double>();

            foreach (var supplier in Suppliers)
            {
                double totalCost = 0;
                foreach (var supply in supplier.Supplies)
                {
                    totalCost += supply.Price;
                }
                result.Add(supplier.Title, totalCost);
            }
            StringBuilder sb = new StringBuilder();

            foreach (var item in result)
            {
                sb.AppendFormat("{0} - {1}{2}", item.Key, item.Value, Environment.NewLine);
            }

            string resultLine = sb.ToString().TrimEnd();//when converting to string we also want to trim the redundant new line at the very end
            MessageBox.Show(resultLine);
        }

        public ObservableCollection<Supplier> Suppliers { get; set; }

        public SupplierViewModel()
        {
            Suppliers = new ObservableCollection<Supplier>(storage.GetAllSuppliers());
        }
    }
}
