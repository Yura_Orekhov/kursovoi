﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.ViewModel
{
    using DAL;
    using Model;
    using System.Collections.ObjectModel;
    using System.Windows.Input;

    class SupplyGoodViewModel : ViewModelBase
    {
        private Storage storage = new Storage();

        private Supply currentSupply;
        
        public List<Good> Goods { get; set; }

        private ObservableCollection<Good> supplyGoods;
        public ObservableCollection<Good> SupplyGoods
        {
            get { return supplyGoods; }
            set
            {
                supplyGoods = value;
                NotifyPropertyChanged("SupplyGoods");
            }
        } 

        private void LoadGoods(int id)
        {
            Goods = new List<Good>();
            List<Category> categories = storage.GetAllCategories();

            foreach (var cat in categories)
            {
                foreach (var g in cat.Goods)
                {
                    Goods.Add(g);
                }
            }

            var Supplies = new ObservableCollection<Supply>();
            List<Supplier> suppliers = storage.GetAllSuppliers();

            foreach (var supplier in suppliers)
            {
                foreach (var supply in supplier.Supplies)
                {
                    Supplies.Add(supply);
                }
            }
            currentSupply = Supplies.Where(p => p.ID == id).FirstOrDefault();

            SupplyGoods = new ObservableCollection<Good>();
            foreach(var supplyGood in currentSupply.GoodSupply)
            {
                SupplyGoods.Add(Goods.Where(p => p.ID == supplyGood.GoodId).FirstOrDefault());
            }
        }

        private Good selectedGood;
        public Good SelectedGood
        {
            get { return selectedGood; }
            set
            {
                selectedGood = value;
                NotifyPropertyChanged("SelectedGood");
            }
        }

        private ICommand addGoodCommand;
        public ICommand AddGoodCommand
        {
            get
            {
                if (addGoodCommand == null)
                    addGoodCommand = new RelayCommand(
                        p => AddGood(),
                        p => true
                        );
                return addGoodCommand;
            }
        }

        private void AddGood()
        {
            if (selectedGood == null) return;
            SupplyGoods.Add(SelectedGood);

            var suppliers = storage.GetAllSuppliers();
            suppliers.Where(p => p.ID == currentSupply.Supplier.ID).First().Supplies.Where(p => p.ID == currentSupply.ID).
                First().GoodSupply.Add(new GoodSupply() { GoodId = SelectedGood.ID, GoodTitle = selectedGood.Title });
            storage.SaveSuppliers(suppliers);
        }

        public SupplyGoodViewModel(int supplyId)
        {
            LoadGoods(supplyId);
        }
    }
}
