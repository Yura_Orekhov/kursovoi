﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.ViewModel
{
    using DAL;
    using Model;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;
    using Views;

    class SupplyViewModel : ViewModelBase
    {
        private Storage storage = new Storage();

        private ObservableCollection<Supply> supplies;
        public ObservableCollection<Supply> Supplies
        {
            get { return supplies; }
            set
            {
                supplies = value;
                NotifyPropertyChanged("Supplies");
            }
        }   
        public List<Supplier> Suppliers { get; set; } 
        public List<Good> Goods { get; set; }

        private Supply newSupply = new Supply();
        public Supply NewSupply
        {
            get { return newSupply; }
            set
            {
                newSupply = value;
                NotifyPropertyChanged("NewSupply");
            }
        }

        private ICommand addSupplyCommand;
        public ICommand AddSupplyCommand
        {
            get
            {
                if (addSupplyCommand == null)
                    addSupplyCommand = new RelayCommand(
                        p => AddSupply(),
                        p => true
                        );
                return addSupplyCommand;
            }
        }

        private Good selectedGood;
        public Good SelectedGood
        {
            get { return selectedGood; }
            set
            {
                selectedGood = value;
                NotifyPropertyChanged("SelectedGood");
            }
        }

        private Supply selectedSupply;
        public Supply SelectedSupply
        {
            get { return selectedSupply; }
            set
            {
                selectedSupply = value;
                NotifyPropertyChanged("SelectedSupply");
            }
        }

        private ICommand showGoodsCommand;
        public ICommand ShowGoodsCommand
        {
            get
            {
                if (showGoodsCommand == null)
                    showGoodsCommand = new RelayCommand(
                        p => ShowGoods(),
                        p => true
                        );
                return showGoodsCommand;
            }
        }

        private void ShowGoods()
        {
            if (SelectedSupply == null) return;
            supplyGoods wnd = new supplyGoods();
            wnd.DataContext = new SupplyGoodViewModel(SelectedSupply.ID);
            wnd.Closed += (en, es) =>
             {
                 LoadSupplies();
             };
            wnd.ShowDialog();
            
        }

        private void AddSupply()
        {
            if (!NewSupply.IsValid) return;
            bool isUnique = false;
            while (isUnique == false)
            {
                Random r = new Random();
                int id = r.Next(10000000, 99999999);
                var query = Supplies.Where(p => p.ID == id).ToList();
                if (query.Count == 0)
                {
                    isUnique = true;
                    NewSupply.ID = id;
                }
            }
            try
            {
                var cat = storage.GetAllSuppliers();
                cat.Where(p => p.ID == NewSupply.Supplier.ID).FirstOrDefault().Supplies.Add(NewSupply);
                storage.SaveSuppliers(cat.ToList());
                Supplies.Add(NewSupply);
                LoadSupplies();
                NewSupply = new Supply();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadSupplies()
        {
            Supplies = new ObservableCollection<Supply>();
            List<Supplier> suppliers = storage.GetAllSuppliers();

            foreach (var supplier in suppliers)
            {
                foreach (var supply in supplier.Supplies)
                {
                    Supplies.Add(supply);
                }
            }
        }

        private ICommand removeCommand;
        public ICommand RemoveCommand
        {
            get
            {
                if (removeCommand == null)
                    removeCommand = new RelayCommand(
                        p => Remove(),
                        p => true
                        );
                return removeCommand;
            }
        }


        private void Remove()
        {
            if (selectedSupply == null) return;

            try
            {
                var suppliers = storage.GetAllSuppliers();
                var supply =  suppliers.Where(p => p.ID == selectedSupply.Supplier.ID).First().
                    Supplies.Where(p => p.ID == selectedSupply.ID).First();
                suppliers.Where(p => p.ID == selectedSupply.Supplier.ID).First().
                    Supplies.Remove(supply);
                Supplies.Remove(selectedSupply);
                storage.SaveSuppliers(suppliers);
                MessageBox.Show("Поставка була відмінена!!!");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private ICommand refreshCommand;
        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand == null)
                    refreshCommand = new RelayCommand(
                        p => Refresh(),
                        p => true
                        );
                return refreshCommand;
            }
        }

        private void Refresh()
        {
            LoadSupplies();
        }

        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                    saveCommand = new RelayCommand(
                        p => Save(),
                        p => true
                        );
                return saveCommand;
            }
        }

        private void Save()
        {
            try
            {
                storage.SaveSuppliers(Suppliers.ToList());
                MessageBox.Show("Дані збережено!!!");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public SupplyViewModel()
        {
            LoadSupplies();
            Suppliers = new List<Supplier>(storage.GetAllSuppliers());
        }
    }
}
