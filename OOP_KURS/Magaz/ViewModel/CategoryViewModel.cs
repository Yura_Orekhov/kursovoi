﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.ViewModel
{
    using Model;
    using System.Collections.ObjectModel;
    using DAL;
    using System.Windows.Input;
    using System.Windows;

    class CategoryViewModel : ViewModelBase
    {
        private Storage storage = new Storage();

        private Category newCategory;
        public Category NewCategory
        {
            get { return newCategory; }
            set
            {
                newCategory = value;
                NotifyPropertyChanged("NewCategory");
            }
        }

        private ICommand addCategoryCommand;
        public ICommand AddCategoryCommand
        {
            get
            {
                if (addCategoryCommand == null)
                    addCategoryCommand = new RelayCommand(
                        p => AddCategory(),
                        p => true
                        );
                return addCategoryCommand;
            }
        }

        private void AddCategory()
        {
            if (!newCategory.IsValid) return;
            bool isUnique = false;
            while(isUnique == false)
            {
                Random r = new Random();
                int id = r.Next(10000000, 99999999);
                var query = Categories.Where(p => p.ID == id).ToList();
                if (query.Count == 0)
                {
                    isUnique = true;
                    NewCategory.ID = id;
                }
            }
            try
            {
                Categories.Add(NewCategory);
                NewCategory = new Category();
                storage.SaveCategories(Categories.ToList());
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                    saveCommand = new RelayCommand(
                        p => Save(),
                        p => true
                        );
                return saveCommand;
            }
        }

        private void Save()
        {
            try
            {
                storage.SaveCategories(Categories.ToList());
                MessageBox.Show("Дані збережено!!!");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private ICommand refreshCommand;
        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand == null)
                    refreshCommand = new RelayCommand(
                        p => Refresh(),
                        p => true
                        );
                return refreshCommand;
            }
        }

        public void Refresh()
        {
            Categories = new ObservableCollection<Category>(storage.GetAllCategories());
        }

        private Category selectedCategory;
        public Category SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                selectedCategory = value;
                NotifyPropertyChanged("SelectedCategory");
            }
        }

        private ICommand removeCommand;
        public ICommand RemoveCommand
        {
            get
            {
                if (removeCommand == null)
                    removeCommand = new RelayCommand(
                        p => Remove(),
                        p => true
                        );
                return removeCommand;
            }
        }

        private void Remove()
        {
            if (selectedCategory == null) return;

            try
            {
                var suppliers = storage.GetAllSuppliers();

                foreach (var supplier in suppliers)
                {
                    foreach (var supplie in supplier.Supplies)
                    {
                        foreach (var good in supplie.GoodSupply.ToArray())
                        {
                            foreach (var go in selectedCategory.Goods)
                            {
                                if (good.GoodId == go.ID)
                                    supplie.GoodSupply.Remove(good);
                            }
                        }
                    }
                }

                storage.SaveSuppliers(suppliers);

                var categories = storage.GetAllCategories();
                var category = categories.Where(p => p.ID == selectedCategory.ID).First();
                categories.Remove(category);
                storage.SaveCategories(categories);
                Categories.Remove(SelectedCategory);
                MessageBox.Show("Категорія і всі товари були видалені!!!");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            

        }

        public ObservableCollection<Category> Categories { get; set; }

        public CategoryViewModel()
        {
            Categories = new ObservableCollection<Category>(storage.GetAllCategories());
            NewCategory = new Category();
        }
    }
}
