﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.ViewModel
{
    using Model;
    using DAL;
    using System.Windows.Input;
    using System.Windows;
    using System.Collections.ObjectModel;
    using Views;

    public class GoodViewModel : ViewModelBase
    {
        private Storage storage = new Storage();

        private ObservableCollection<Good> goods;
        public ObservableCollection<Good> Goods
        {
            get { return goods; }
            set
            {
                goods = value;
                NotifyPropertyChanged("Goods");
            }
        }

        public ObservableCollection<Category> Categories { get; set; }

        private Good good;
        public Good Good
        {
            get { return good; }
            set
            {
                good = value;
                NotifyPropertyChanged("Good");
            }
        }

        private string searchCriteria;
        public string SearchCriteria
        {
            get { return searchCriteria; }
            set { searchCriteria = value; NotifyPropertyChanged("SearchCriteria"); }
        }

        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get
            {
                if (searchCommand == null)
                    searchCommand = new RelayCommand(
                        p => Search(),
                        p => true
                        );
                return searchCommand;
            }
        }

        private void Search()
        {
            if (string.IsNullOrEmpty(SearchCriteria)) return;
            LoadGoods();
            Goods = new ObservableCollection<Good>(Goods.Where(p => p.Title.Contains(SearchCriteria)));
        }


        private ICommand refreshCommand;
        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand == null)
                    refreshCommand = new RelayCommand(
                        p => Refresh(),
                        p => true
                        );
                return refreshCommand;
            }
        }
        private void Refresh()
        {
            LoadGoods();
        }

       

        private ICommand addGoodCommand;
        public ICommand AddGoodCommand
        {
            get
            {
                if (addGoodCommand == null)
                    addGoodCommand = new RelayCommand(
                        p => AddGood(),
                        p => true
                        );
                return addGoodCommand;
            }
        }

        private void AddGood()
        {
            if (!Good.IsValid) return;
            bool isUnique = false;
            while (isUnique == false)
            {
                Random r = new Random();
                int id = r.Next(10000000, 99999999);
                var query = Goods.Where(p => p.ID == id).ToList();
                if (query.Count == 0)
                {
                    isUnique = true;
                    Good.ID = id;
                }
            }
            try
            {
                var cat = storage.GetAllCategories();
                cat.Where(p => p.ID == Good.Category.ID).FirstOrDefault().Goods.Add(Good);
                storage.SaveCategories(cat.ToList());
                Goods.Add(Good);
                LoadGoods();
                Good = new Good();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private Good selectedGood;
        public Good SelectedGood
        {
            get { return selectedGood; }
            set
            {
                selectedGood = value;
                NotifyPropertyChanged("SelectedGood");
            }
        }

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                    deleteCommand = new RelayCommand(
                        p => Delete(),
                        p => true
                        );
                return deleteCommand;
            }
        }

        private void Delete()
        {
            if (selectedGood == null) return;
            try
            {
                var categories = storage.GetAllCategories();
               var good =  categories.Where(p => p.ID == selectedGood.Category.ID).First().
                    Goods.Where(p => p.ID == selectedGood.ID).First();

                var suppliers = storage.GetAllSuppliers();

                foreach (var supplier in suppliers)
                {
                    foreach (var supplie in supplier.Supplies)
                    {
                        foreach (var goodd in supplie.GoodSupply.ToArray())
                        {
                            
                                if (goodd.GoodId == SelectedGood.ID)
                                    supplie.GoodSupply.Remove(goodd);
                            
                        }
                    }
                }
                storage.SaveSuppliers(suppliers);
                var flag = categories.Where(p => p.ID == selectedGood.Category.ID).First().
                    Goods.Remove(good);
                storage.SaveCategories(categories);
                Goods.Remove(selectedGood);
                MessageBox.Show("Товар видалено!!!");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void LoadGoods()
        {
            Goods = new ObservableCollection<Good>();
            List<Category> categories = storage.GetAllCategories();

            foreach(var cat in categories)
            {
                foreach(var g in cat.Goods)
                {
                    Goods.Add(g);
                }
            }
        }

        private ICommand saveChangesCommand;
        public ICommand SaveChangesCommand
        {
            get
            {
                if (saveChangesCommand == null)
                    saveChangesCommand = new RelayCommand(
                        p => Save(),
                        p => true
                        );   
                return saveChangesCommand;
            }
        }

        private void Save()
        {
            try
            {
                var categories = storage.GetAllCategories();
                foreach(var category in categories.ToArray())
                {
                    category.Goods = Goods.Where(p => p.Category.ID == category.ID).ToList();
                }
                storage.SaveCategories(categories);
                MessageBox.Show("Дані збережено!!!");

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public GoodViewModel()
        {
            LoadGoods();
            Categories = new ObservableCollection<Category>(storage.GetAllCategories());
            Good = new Good();
        } 
    }
}
