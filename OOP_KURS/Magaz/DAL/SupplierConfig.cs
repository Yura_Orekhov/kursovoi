﻿using ExtendedXmlSerialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.DAL
{
    using Model;
    class SupplierConfig : ExtendedXmlSerializerConfig<Supplier>
    {
        public SupplierConfig()
        {
            ObjectReference(p => p.ID);
        }
    }
}
