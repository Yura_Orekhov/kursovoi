﻿using ExtendedXmlSerialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.DAL
{
    using Model;

    class CategoryConfig : ExtendedXmlSerializerConfig<Category>
    {
        public CategoryConfig()
        {
            ObjectReference(p => p.ID);
        }
    }

}
