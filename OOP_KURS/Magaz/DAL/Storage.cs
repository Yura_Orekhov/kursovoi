﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.DAL
{
    using ExtendedXmlSerialization;
    using Model;
    using System.IO;
    using System.Xml;

    public class Storage
    {
        private static string CATEGORY_FILE_PATH = "Categories.xml";
        private static string SUPPLIER_FILE_PATH = "Suppliers.xml";

        public void SaveSuppliers(List<Supplier> suppliers)
        {
            var toolsFactory = new SimpleSerializationToolsFactory();
            toolsFactory.Configurations.Add(new SupplierConfig());
            ExtendedXmlSerializer serializer = new ExtendedXmlSerializer(toolsFactory);
            var xml = serializer.Serialize(suppliers);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.Save(SUPPLIER_FILE_PATH);
        }

        public List<Supplier> GetAllSuppliers()
        {
            string xmlText = File.ReadAllText(SUPPLIER_FILE_PATH);
            var toolsFactory = new SimpleSerializationToolsFactory();
            toolsFactory.Configurations.Add(new SupplierConfig());
            ExtendedXmlSerializer serializer = new ExtendedXmlSerializer(toolsFactory);
            var c = serializer.Deserialize<List<Supplier>>(xmlText);
            return c;
        }

        public void SaveCategories(List<Category> categories)
        {
            var toolsFactory = new SimpleSerializationToolsFactory();
            toolsFactory.Configurations.Add(new CategoryConfig());
            ExtendedXmlSerializer serializer = new ExtendedXmlSerializer(toolsFactory);
            var xml = serializer.Serialize(categories);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.Save(CATEGORY_FILE_PATH);
        }

        public List<Category> GetAllCategories()
        {
            string xmlText = File.ReadAllText(CATEGORY_FILE_PATH);
            var toolsFactory = new SimpleSerializationToolsFactory();
            toolsFactory.Configurations.Add(new CategoryConfig());
            ExtendedXmlSerializer serializer = new ExtendedXmlSerializer(toolsFactory);
            var c = serializer.Deserialize<List<Category>>(xmlText);
            return c;
        }

        public Storage()
        {

        }

    }
}
